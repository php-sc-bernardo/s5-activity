<?php


session_start();

class TaskList {

	//method ifor adding a new task to the task list
	public function add($description){

		// create a $newTask object based on the task added by the user. isFinished defaults to false
		$newTask = (object)[
			'description' => $description,
			'isFinished' => false
		];

		// $_SESSION is anotherkind of super global variable typically used by yhe server to send data in its responses.

		//check if $_SESSION['tasks'] array exist. If it does not yet exist, create it.
		if($_SESSION['tasks'] === null){
			$_SESSION['tasks'] = array(); // if the $_SESSION['tasks'] array 
		}

		// push the $newTask object to the $_SESSION['tasks'] array
		array_push($_SESSION['tasks'], $newTask);

	}

	public function update($id, $description, $isFinished){
		$_SESSION['tasks'][$id]->description = $description;
		$_SESSION['tasks'][$id]->isFinished = ($isFinished !== null) ? true : false;
	}

	public function remove($id){
		array_splice($_SESSION['tasks'], $id, 1);
	}

}

$taskList = new TaskList(); //create a new taskList object

if($_POST['action'] === 'add'){
	$taskList->add($_POST['description']); // call the add methhod from taskList and pass the task decription to it.
} else if($_POST['action'] === 'update'){
	$taskList->update($_POST['id'], $_POST['description'], $_POST['isFinished']);
} else if($_POST['action'] === 'remove'){
	$taskList->remove($_POST['id']);
} else if($_POST['action'] === 'clear'){
	session_destroy();
}

//redirect to the user back to index
header('Location: ./index.php');

